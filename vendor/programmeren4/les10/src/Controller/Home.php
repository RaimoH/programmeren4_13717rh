<?php
namespace Programmeren4\Les10\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    public function index()
    {
        return $this->view('Home','Index', null);
    }
}