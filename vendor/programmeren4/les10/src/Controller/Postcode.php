<?php
namespace Programmeren4\Les10\Controller;

class Postcode extends \ModernWays\Mvc\Controller
{
    public function ShowPostcodes()
    {
        $textPostcodes = file_get_contents('les5/data/Postcodes.csv');
        $lines = explode("\n", $textPostcodes);
        
        $model = new \Programmeren4\Les10\Model\Postcode();
        $postcodes = array();
        
        foreach ($lines as $line) {
            $postcodes[] =  explode('|', $line);
        }
        
        $model->setList($postcodes);
        return $this->view('Postcode','ShowPostcodes', $model);
    }
}