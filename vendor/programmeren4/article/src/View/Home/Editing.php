<main>
    <header>
        <h1>Garage - Programmeren 4</h1>
    </header>
    <article>
        <nav>
            <a href="/article.php/Home/inserting">Inserting</a>
            <a href=""></a>
        </nav>
        <section>
            <?php
                if (count($model->getList()) > 0) { ?>
                <table>
                    <?php foreach($model->getList() as $item) { ?>
                    <tr>
                        <td>
                            <?php echo $item['Name'];?>
                        </td>
                        <td>
                            <?php echo $item['PurchaseDate'];?>
                        </td>
                        <td>
                            <?php echo $item['Price'];?>
                        </td>
                        <td><a href="/article.php/Home/Updating/<?php echo $item['Id'];?>">Update</a></td>
                        <td>Delete</td>
                    </tr>
                    <?php
                    }
                    ?>
                </table>
                <?php
                } else { ?>
                    <p>Geen artikels gevonden</p>
                <?php 
                } ?>
        </section>
    </article>
    <footer></footer>
</main>
