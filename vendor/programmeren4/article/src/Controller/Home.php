<?php
namespace Programmeren4\Article\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    private $pdo;
    private $model;

    public function __construct()
    {
        $this->pdo = new \PDO('mysql:host=localhost;dbname=raimohuybrechts;charset=utf8', 'digifais', '');
        $this->model = new \Programmeren4\Article\Model\Article();
    }
    
    public function editing()
    {
        // het model vullen
        $command = $this->pdo->query("call ArticleSelectAll");
        // associatieve array kolomnamen en waarde per rij
        $this->model->setList($command->fetchAll(\PDO::FETCH_ASSOC));
        return $this->view('Home','Editing', $this->model);
    }
    
    public function inserting()
    {
        return $this->view('Home', 'Inserting', null);
    }
    
    public function insert()
    {
        $this->model->setName($_POST['ArticleName']);
        $this->model->setPurchaseDate($_POST['ArticlePurchaseDate']);
        $this->model->setPrice($_POST['ArticlePrice']);
        $statement = $this->pdo->prepare("call ArticleInsert(:pName, 
            :pPurchaseDate, :pPrice, @pId)");
        // bindValue is by reference
        // bindParam is by value
        $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
        $statement->bindValue(':pPurchaseDate', $this->model->getPurchaseDate(), \PDO::PARAM_STR);
        $statement->bindValue(':pPrice', $this->model->getPrice(), \PDO::PARAM_STR);
        $result = $statement->execute();
        // zet de nieuw toegekende Id in het Id veld van het model
        // $this->model->setId($pdo->query('select @pId')->fetchColumn());
        return $this->editing();
    }
    
    public function updating()
    {
        $this->model->setId($this->routeId);
        $statement = $this->pdo->prepare("call ArticleSelectOne(:pId)");
        $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
        $articleOne = $statement->fetch(\PDO::FETCH_ASSOC);
        $this->model->setName($articleOne['Name']);
        $this->model->setPurchaseDate($articleOne['PurchaseDate']);
        $this->model->setPrice($articleOne['Price']);
        return $this->view('Home', 'Updating', $this->model);
    }
}