<?php
/* Class: Article
 * programmeren4
 * 
 * Model for Article
 * FileName: programmeren4/Article/src/Model/Article.php
 * Created on Sunday 19thh of December 2016 02:53:28 PM
*/
namespace Programmeren4\Article\Model;
class Article extends \ModernWays\Mvc\Model
{
    private $id;
    private $name;
    private $purchaseDate;
    private $price;
    
    /**
    * @return mixed
    */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }   
    
    /**
    * @return mixed
    */
    public function getName()
    {
        return $this->name;
    }
    
    /**
    * @return mixed
    */
    public function getNameUpperCase()
    {
        return strtoupper($this->name);
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }  
    
        /**
    * @return mixed
    */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * @param mixed $purchaseDate
     */
    public function setPurchaseDate($purchaseDate)
    {
        $this->purchaseDate = $purchaseDate;
    }  
    
    /**
    * @return mixed
    */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }  
    
    /**
    * @return mixed
    */
    public function getVATIncluded()
    {
        return $this->price * 1.21;
    }
    
}