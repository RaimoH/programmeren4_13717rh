<?php
    try {
        $pdo = new \PDO('mysql:host=localhost;dbname=raimohuybrechts;charset=utf8', 'digifais', '');
        echo 'Connectie gemaakt.';
        // $command = $pdo->query("SELECT * FROM Article");
        // // associatieve array kolomnamen en waarde per rij
        // $articles = $command->fetchAll(\PDO::FETCH_ASSOC);
        // $command = $pdo->query("call ArticleSelectAll");
        // // associatieve array kolomnamen en waarde per rij
        // $articlesOrdered = $command->fetchAll(\PDO::FETCH_ASSOC);
        // $command = $pdo->query("call ArticleSelectOne(1)");
        // // associatieve array kolomnamen en waarde per rij
        // $articleOne = $command->fetchAll(\PDO::FETCH_ASSOC);
        // //Onveilige manier
        // $result = $pdo->exec("call ArticleInsert('Kangoo', '2016-12-19', 20.45, @pId)");
        // $result = $pdo->exec("call ArticleUpdate('BMW', '2016-12-19', 20.45, 3)");
        // $result = $pdo->exec("call ArticleDelete(1)");
        // insert met placeholders
        $statement = $pdo->prepare("call ArticleInsert(:pName, :pPurchaseDate, :pPrice, @pId)");
        // bindValue is by reference
        // bindParam is by value
        $statement->bindValue(':pName', 'Scénic', \PDO::PARAM_STR);
        $statement->bindValue(':pPurchaseDate', '2016-12-19', \PDO::PARAM_STR);
        $statement->bindValue(':pPrice', '300.56', \PDO::PARAM_STR);
        $result = $statement->execute();
        $newId = $pdo->query('select @pId')->fetchColumn();

        $command = $pdo->query("call ArticleSelectAll");
        // associatieve array kolomnamen en waarde per rij
        $articlesOrdered = $command->fetchAll(\PDO::FETCH_ASSOC);
    }
    catch (\PDOException $e) {
        echo $e->getMessage();
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Leren werken met PDO</title>
</head>
<body>
    <h1>Ongeordend</h1>
    <pre>
        <?php var_dump($articles);?>
    </pre>
    <h1>Geordend</h1>
    <pre>
        <?php var_dump($articlesOrdered);?>
    </pre>
    <h1>Een artikel</h1>
    <pre>
        <?php var_dump($articleOne);?>
    </pre>
</body>
</html>