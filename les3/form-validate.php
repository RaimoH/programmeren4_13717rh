<?php
    $productError = '';
    $priceError = '';
    if (isset($_POST['product'])) {
        $productName = $_POST['product'];
        if(empty(productName)) {
            $productError = 'Product moet een naam hebben!';
        }
        else {
            $productPattern = '/^[a-zA-Zéç]*$/';
            if (!preg_match($productPattern, $productName)) {
                $productError = 'Naam bestaat uit karakters alleen!';
            }
        }
    };
    
    if (isset($_POST['price'])) {
        $price = $_POST['price'];
        if(empty($price)) {
            $productError = 'Product moet een prijs hebben!';
        }
        else {
            $pricePattern = '/^[0-9]+(\.[0-9]{1,2})?$/';
            if (!preg_match($pricePattern, $price)) {
                $priceError = 'Typ een geldig eurogetal in! Bv. 99.00';
            }
        }
    }
    
    if (isset($_POST['gender'])) {
        $gender = $_POST['gender'];
        switch ($gender) {
            case 0 :
                $genderText = "man";
                break;
            case 1 :
                $genderText = "female";
                break;
            case 2 :
                $genderText = "other";
                break;
            default :
                $genderText = "unknown";
                break;
        }
    }
    
    if (isset($_POST['category'])) {
        $category = $_POST['category'];
        switch ($category) {
            case 0 :
                $categoryText = "Huishoudgerief";
                break;
            case 1 :
                $categoryText = "Elektronica";
                break;
            case 2 :
                $categoryText = "Snoep";
                break;
            case 3 :
                $categoryText = "Boeken";
                break;
            default :
                $categoryText = "Unknown";
                break;
        }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Oefening</title>
</head>
<body>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
        <label for="product">Product:</label>
        <input type="text" id="product" name="product" required/>
        <span><?php echo $productError;?></span>
        </br>
        <label for="price">Price:</label>
        <input type="text" id="price" name="price" required pattern="^[0-9]+(\.[0-9]{1,2})?$"/>
        <span><?php echo $priceError;?></span>
        </br>
        <label for="man">Man</label>
        <input type="radio" id="man" name="gender" value="0"/>
        <label for="female">Female</label>
        <input type="radio" id="female" name="gender" value="1"/>
        <label for="other">Other</label>
        <input type="radio" id="other" name="gender" value="2"/>
        </br>
        <label for="category">Category:</label><select name="category" id="category">
            <option value="0">Huishoudgerief</option>
            <option value="1">Elektronica</option>
            <option value="2">Snoep</option>
            <option value="3">Boeken</option>
        </select>
        </br>
        <button type="submit">Verzenden</button>
    </form> 
    <div>
        <p><?php echo "Je bent een {$genderText}";?></p>
        <p><?php echo "Je bent een {$categoryText}";?></p>
    </div>
</body>
</html>