<?php
    $voornaamError = '';
    $naamError = '';
    $plaatsError = '';
    $aantalPersError = '';
    $geboortedatumError = '';
    $rekeningnrError = '';
    $mailError = '';
    
    if (isset($_POST['voornaam'])) {
        $voornaamName = $_POST['voornaam'];
        if(empty(voornaamName)) {
            $voornaamError = 'Voornaam mag niet leeg zijn!';
        }
        else {
            $voornaamPattern = '/^[a-zA-Zéç]*$/';
            if (!preg_match($voornaamPattern, $voornaamName)) {
                $voornaamError = 'Naam bestaat uit karakters alleen!';
            }
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div>
        <form action="behandelRegistratie.php" method="post">
            <h1>Bob Dylan</h1>
            <div>
                <label for="voornaam">Voornaam: </label>
                <input type="text" id="voornaam">
                <span><?php echo $voornaamError;?></span>
            </div>
            <div>
                <label for="naam">Naam: </label>
                <input type="text" id="naam">
            </div>
            <div>
                <label for="plaats">Plaats: </label>
                <select name="plaats" id="plaats">
                    <option value="0">Staanplaats</option>
                    <option value="1">Tribune</option>
                    <option value="2">Balkon</option>
                </select>
            </div>
            <div>
                <label for="aantalPers">Aantal personen: </label>
                <input type="text" id="aantalPers">
            </div>
            <div>
                <label for="geboortedatum">Geboortedatum: </label>
                <input type="date" id="geboortedatum">
            </div>
            <div>
                <label for="rekeningnr">Rekeningnummer: </label>
                <input type="text" id="rekeningnr" placeholder="BE123456789090" data-pattern="iban">
            </div>
            <div>
                <label for="mail">Email: </label>
                <input type="email" id="mail" placeholder="someone@example.com">
            </div>
            <div>
                <button type="submit">Verzenden</button>
            </div>
        </form>
    </div>
</body>
</html>