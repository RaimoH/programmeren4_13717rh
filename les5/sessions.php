<?php 
    session_start(); 

    if(isset($_POST['product'])){
        if(isset($_SESSION['cart'])){
            $cart = unserialize($_COOKIE['cart']);
        }

    $cart [] = $_POST['product'];

    setsession("cart", serialize($cart),time()+(86400 * 7));
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Webwinkel</title>
</head>
<body>
    <h1>Webwinkel</h1>
    <p>Hieronder vind je een lijst van de goederen die we verkopen</p>
    <label for="">Product</label>
    <form method="post">
        <select id="product" name="product">
            <option value="1">Game 1</option>
            <option value="2">Game 2</option>
            <option value="3">Game 3</option>
            <option value="4">Game 4</option>
        </select>
        <button type="submit">Kopen</button>
    </form>
    <div>
        <?php 
        if(isset($_COOKIE['cart'])){
            $cart = unserialize($_COOKIE['cart']);
            foreach ($cart as $value) {
                echo $value.'<br/>';
            }
        }
        else {
            echo 'Je cart is leeg.';
        }
        ?>
    </div>
</body>
</html>